import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { IonicPage, Loading, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { HomePage } from '../home/home';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
	name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class Login {
  username:any = "admin@notif.com";
  password:any = "logitech";
  result: any;
  loading:any;


	public loginForm:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, 
        EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), 
        Validators.required])]
    });
  }

  	loginUser(): void {
	  if (!this.loginForm.valid){
	    console.log(this.loginForm.value);
	  } else {
	    // this.authProvider.loginUser(this.loginForm.value.email, 
	    //     this.loginForm.value.password)
	    // .then( authData => {
	    //   this.loading.dismiss().then( () => {
	    //     this.navCtrl.setRoot(HomePage);
	    //   });
	    // }, error => {
	    //   this.loading.dismiss().then( () => {
	    //     let alert = this.alertCtrl.create({
	    //       message: error.message,
	    //       buttons: [
	    //         {
	    //           text: "Ok",
	    //           role: 'cancel'
	    //         }
	    //       ]
	    //     });
	    //     alert.present();
	    //   });
	    // });
	    // this.loading = this.loadingCtrl.create();
	    // this.loading.present();
	  }
	}

	goToResetPassword(): void { this.navCtrl.push('reset-password'); }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  login() {
    this.loading = this.loadingCtrl.create({
      content: 'Logging in...'
    });
    this.loading.present();
    this.http.post("http://192.168.1.10:8100/api/login",this.getFormUrlEncoded({email:this.username,password:this.password}),{headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})}).map(res => res.json()).subscribe(
      data => this.result = data,
      err => {
        console.log('Error');
        this.loading.dismiss();
        if(err.json().error == "Unauthorised")
        {
          console.log("Unauthorised");
          let toast = this.toastCtrl.create({
            message:  'Incorrect ID or password!',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          this.password = null;
        }
        else{
          let toast = this.toastCtrl.create({
            message:  'Server Error. Try again later!',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      () => {
        console.log('Success');
        localStorage.setItem("auth_token", this.result.success.token);
        this.loading.dismiss();
        this.navCtrl.setRoot(HomePage);
      }
    );
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }
}
