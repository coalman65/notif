import { Component } from '@angular/core';
import { App, NavController, PopoverController, LoadingController, ToastController } from 'ionic-angular';
import { Global } from '../../providers/global';
import { PopoverPage } from '../popover/popover';
import { TruncatePipe } from '../../pipes/truncate';
import { Notif } from '../notif/notif';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Login } from '../login/login';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private isOn: boolean = false;
  loading:any;
  notifs:any;

  constructor(public app: App, public navCtrl: NavController, public global: Global, public popCtrl: PopoverController,
  public loadingCtrl: LoadingController, public toastCtrl: ToastController, public http: Http) {
  	
  }

  doRefresh(refresher) {
   console.log('Refresh');
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentPopover(myEvent) {
    //let popover = this.popCtrl.create(PopoverPage,{},{ cssClass: 'custom-popover'});
    let popover = this.popCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  toggleDetails() {
    this.isOn = !this.isOn;
  }

  gotonotif() {
    this.navCtrl.push(Notif);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home');
    this.request();
  }

  request() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading notifications...'
    });
    this.loading.present();

    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem("auth_token"));

    this.http.get("http://192.168.1.10:8100/api/notifs",{headers: headers}).map(res => res.json()).subscribe(
      data => this.notifs = data.notifs,
      err => {
        console.log('Error');
        this.loading.dismiss();
        if(err.json().error == "Unauthorised")
        {
          console.log("Unauthorised");
          this.app.getActiveNav().setRoot(Login);
        }
        else{
          let toast = this.toastCtrl.create({
            message:  'Server Error. Try again later!',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      () => {
        console.log('Notifications loaded');
        this.loading.dismiss();
        //console.log(this.notifs);
      }
    );
  }


  // getButtonText(): string {
  //   return `Switch ${ this.isOn ? 'Off' : 'On' }`;
  // }
  // setState(): void {
  //   this.isOn = !this.isOn;
  // }

}
