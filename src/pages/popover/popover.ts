import { Component } from '@angular/core';
import { App, NavParams, NavController, ToastController, ViewController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Login } from '../login/login';
@Component({
  selector: 'page-popover',
  template: `
    <ion-list no-lines style="margin-bottom:0">
    	<button ion-item (click)="web()" >Bigular Web</button>
    	<button ion-item (click)="logout()" >Logout</button>
    </ion-list>
  `,
})
export class PopoverPage {
  

  constructor(public app: App, public viewCtrl: ViewController, private navParams: NavParams, public navCtrl: NavController, public socialSharing: SocialSharing, public toastCtrl: ToastController) {
  }

  logout()
  {
    console.log("You have been logged out");
    this.viewCtrl.dismiss();
    localStorage.clear();
    this.app.getActiveNav().setRoot(Login);
  }

  // shareApp()
  // {
  //   console.log("Social share activated");
  //   this.viewCtrl.dismiss();
  // 	this.socialSharing.share("Test share Message","Sample Subject","www/assets/sample-icon.png").then(() => {
  // 					console.log("Shared");
  // 				}).catch((err) => {
  // 					console.log(err);
  // 				});
  // }

  web() {
    console.log("Bigular Web");
    this.viewCtrl.dismiss();
    let toast = this.toastCtrl.create({
      message:  'Stay tuned for Bigular Web!',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}