import { Component, Renderer2, ElementRef, AfterViewInit } from '@angular/core';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Platform, LoadingController } from 'ionic-angular';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { SocialSharing } from '@ionic-native/social-sharing';
import { RoundProgressConfig } from 'angular-svg-round-progressbar';


/**
 * Generated class for the Notif page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

declare var refreshMedia;

@Component({
  selector: 'page-notif',
  templateUrl: 'notif.html',
})
export class Notif implements AfterViewInit{

  loader:any;
  current:number[]=[];
  fileStatus:boolean[]=[];
  fileTransfer:FileTransferObject;
  desc:any = "Copy";

  constructor(public photoViewer: PhotoViewer, public rd: Renderer2,public elRef: ElementRef, public file: File, public transfer: FileTransfer, public fileOpener: FileOpener, public loadingCtrl: LoadingController, public socialShare: SocialSharing, private _config: RoundProgressConfig, public platform: Platform) {
    this.loader = loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loader.present();

    _config.setDefaults({
      stroke: 3,
      radius: 9,
      semicircle: false,
      rounded: true,
      responsive: false,
      clockwise: true,
      color: '#45ccce',
      background: '#eaeaea',
      duration: 800,
      animation: 'easeOutCubic',
      animationDelay: 0,
    });    

    this.platform.ready().then(() => {
      this.fileTransfer = this.transfer.create();
    });

    
  }

 
  items: any = {
    'Copy': [ 
      'Director','CSE 1[A]','CSE 1[B]','COE','Dean Academics'
    ],
    'Attach': [
      {
        'title': 'Recruitment_notice.pdf',
        'url': 'url',
        'mime': ''
      },
      {
        'title': 'Recruitment_notice.pdf',
        'url': 'url',
        'mime': ''
      }
    ],
    'Link': [
      {
        title: 'http://sbsstc.ac.in/users/login/getNotices.php',
        url: 'http://sbsstc.ac.in/'
      },
      {
        title: 'http://sbsstc.ac.in/users/login/getNotices.php',
        url: 'http://sbsstc.ac.in/'
      },
      {
        title: 'http://sbsstc.ac.in/users/login/getNotices.php',
        url: 'http://sbsstc.ac.in/'
      }
    ]
  };

  getItems(type:any) {
    return this.items[type];
  }

  showImage() {
    this.photoViewer.show('https://i.pinimg.com/736x/1d/19/86/1d198665331909b115867a0e490e3000--man-suit-ironman.jpg');
  }

  goto(url) {
    window.location.href=url;
  }
  
  hideLoader() {
    this.loader.dismiss();
  }

  shareNotif() {
    this.socialShare.share("hello", "subject","https://i.pinimg.com/736x/1d/19/86/1d198665331909b115867a0e490e3000--man-suit-ironman.jpg" , "https://i.pinimg.com/736x/1d/19/86/1d198665331909b115867a0e490e3000--man-suit-ironman.jpg");
  }

  download(roundId) {
    
    console.log("Download initiated");
  
    this.fileTransfer.onProgress((progressEvent) => {
      if (progressEvent.lengthComputable) {
        //console.log((progressEvent.loaded/progressEvent.total)*100);
        this.current[roundId] = (progressEvent.loaded/progressEvent.total)*100;
      }
    });

    const url = 'https://i.pinimg.com/736x/1d/19/86/1d198665331909b115867a0e490e3000--man-suit-ironman.jpg';
    let path = "notif/"+url.substr(url.lastIndexOf('/')+1);
    this.file.checkFile(this.file.externalRootDirectory, path).then(_ => {
      console.log('File exists');

      this.fileOpener.open(this.file.externalRootDirectory+path, 'image/*')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error openening file', e));    
    }).catch(err => {
      //console.log(err);
      this.rd.removeClass(this.elRef.nativeElement.getElementsByClassName('roundprogress')[roundId],"hidden");
      this.rd.addClass(this.elRef.nativeElement.getElementsByClassName('myicon')[roundId],"hidden");
      
      this.fileTransfer.download(url, this.file.externalRootDirectory+path).then((entry) => {
        refreshMedia.refresh(this.file.externalRootDirectory+path);
        console.log('Download complete: ' + entry.toURL());
        this.rd.addClass(this.elRef.nativeElement.getElementsByClassName('roundprogress')[roundId],"hidden");
        this.rd.removeClass(this.elRef.nativeElement.getElementsByClassName('myicon')[roundId],"hidden");
        this.fileStatus[roundId] = true;
      }, (error) => {
        console.log(error);
      });
    }); 
  }

  fileExists(element,index)
  {
    const url = 'https://i.pinimg.com/736x/1d/19/86/1d198665331909b115867a0e490e3000--man-suit-ironman.jpg';
    let path = "notif/"+url.substr(url.lastIndexOf('/')+1);
    this.file.checkFile(this.file.externalRootDirectory, path).then(_ => {
      this.fileStatus[index] = true;
    }).catch(err => {
      this.fileStatus[index] = false;
    });
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.items.Attach.forEach((element,index) => {
        this.fileExists(element,index);
        this.current[index] = 0;      
      });
      this.hideLoader();
    });
  }

}
