import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  homeRoot = HomePage;

  constructor(public socialSharing: SocialSharing) {

  }

  logout()
  {
  	console.log("You have been logged out");
  }

  shareApp()
  {
  	this.socialSharing.share("Test share Message","Sample Subject","www/assets/sample-icon.png").then(() => {
  					console.log("Shared");
  				}).catch((err) => {
  					console.log(err);
  				});
  }
}
